<?php

namespace Database\Seeders;

use App\Models\film;
use App\Models\User;
use App\Models\genre;
use App\Models\studio;
use Illuminate\Database\Seeder;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();

        // \App\Models\User::factory()->create([
        //     'name' => 'Test User',
        //     'email' => 'test@example.com',
        // ]);

        User::create([
            'name'  => "Apple",
            "email" => "Apple@gmail.com",
            "password" => bcrypt("password"),
            "saldo" => 10000000,
            "role_id"   => 1
        ]);

        // Film Dumy
        for($i = 1; $i <= 20; $i++){
            studio::create([
                'jumlah_kursi'   => 32,
                'nama_studio'   => "Studio". $i++
            ]);

            genre::create([
                'genre' => 'genre'. $i++
            ]);
    
            film::create([
                'judul' => 'Lupa Cara Tidur',
                'sinopsis' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Odio quae iusto odit sit voluptate dolorum alias inventore, natus cupiditate, quis iste rem. Odit accusamus, ratione dolorem doloribus quidem temporibus perferendis expedita praesentium ipsum consequatur id, illo, mollitia sapiente! Eos fugit et ducimus culpa consequatur adipisci. Quasi minima dolore officia amet a numquam dolorem sit tenetur eum debitis ullam iste quos ipsa quisquam dolor consectetur fugit, quas, illum aspernatur vitae sequi? Pariatur eum velit nam nesciunt harum blanditiis dolores reprehenderit odit, hic sed fugit quas commodi eveniet corporis, consectetur, officiis quasi ipsa voluptas earum aperiam nobis reiciendis quidem soluta ab! Odio?',
                'harga_tiket'   => 34000,
                'gambar_film'   => 'gambarDummy.jpeg',
                'durasi'        => "2 jam 15mnt",
                'genre_id'      => 1
            ]);
        }
    }
}
