<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class transaksi extends Model
{
    use HasFactory;

    protected $primaryKey = 'transaksi_id';
    protected $guarded = ['transaksi_id'];
}
