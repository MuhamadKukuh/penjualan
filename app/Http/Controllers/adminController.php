<?php

namespace App\Http\Controllers;

use App\Models\film;
use Inertia\Inertia;
use Illuminate\Http\Request;

class adminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $films = film::paginate()->map(function($film){
            return [
                'id'    => $film->film_id,
                'judul' => $film->judul,
                'gambar_film' => asset('images/'.$film->gambar_film),
                'genre_id'  => $film->genre_id,
                'harga_tiket'=> $film->harga_tiket,
                'sinopsis'  => $film->sinopsis,
                'durasi'    => $film->durasi
            ];
        });

        return Inertia::render('Dashboard', compact('films'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // @dd($request->all());
        // 
        $vali = $request->validate([
            'judul' => "required",
            'sinopsis' => 'required',
            'harga_tiket' => 'required',
            'durasi'    => 'required',
        ]);

        if($request->gambar_film){
            $request->validate(['gambar_film' => 'mimes:jpeg,jpg,png']);

            $imageName = time().'.'.$request->gambar_film->extension();  
     
            $request->gambar_film->move(public_path('images'), $imageName);
            
            film::create([
                'judul' => $request->judul,
                'sinopsis' => $request->sinopsis,
                'harga_tiket' => $request->harga_tiket,
                'durasi'    => $request->durasi,
                'gambar_film'=> $imageName
            ]);
        }else {
            film::create($vali);
        }

        return redirect('/dashboard')->with('success', 'berhasil menambah buku');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $film = film::where('film_id', $id)->first();
        // @dd($id);
        $image = asset('images/'. $film->gambar_film);

        return Inertia::render('EditFilm', compact('film', 'image'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $vali = $request->validate([
            'judul' => "required",
            'sinopsis' => 'required',
            'harga_tiket' => 'required',
            'durasi'    => 'required',
        ]);

        if($request->gambar_film){
            $request->validate(['gambar_film' => 'mimes:jpeg,jpg,png']);

            $imageName = time().'.'.$request->gambar_film->extension();  
     
            $request->gambar_film->move(public_path('images'), $imageName);
            
            film::where('film_id', $id)->update([
                'judul' => $request->judul,
                'sinopsis' => $request->sinopsis,
                'harga_tiket' => $request->harga_tiket,
                'durasi'    => $request->durasi,
                'gambar_film'=> $imageName
            ]);
        }else {
            film::where('film_id', $id)->update($vali);
        }

        return redirect('/dashboard')->with('success', 'berhasil mengedit film');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        film::destroy($id);

        return back()->with('success', 'berhasil menghapus film');
    }
}
