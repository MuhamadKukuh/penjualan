<?php

namespace App\Http\Controllers;

use App\Models\film;
use App\Models\User;
use Inertia\Inertia;
use App\Models\tiket;
use App\Models\studio;
use App\Models\transaksi;
use Illuminate\Http\Request;

class userController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $films = film::paginate(4)->map(function($film){
            return [
                'id'    => $film->film_id,
                'judul' => $film->judul,
                'gambar_film' => asset('images/'. $film->gambar_film),
                'genre_id'  => $film->genre_id,
                'harga_tiket'=> $film->harga_tiket,
                'sinopsis'  => $film->sinopsis,
                'durasi'    => $film->durasi
            ];
        });


        
        return Inertia::render('Home', compact('films'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $film = film::where('film_id', $id)->first();
        $image = asset('images/'.$film->gambar_film);
        $studios = studio::all();
        $user = Auth()->User()->id;
        $recomends = film::paginate(4)->map(function($film){
            return [
                'id'    => $film->film_id,
                'judul' => $film->judul,
                'gambar_film' => asset('images/'.$film->gambar_film),
                'genre_id'  => $film->genre_id,
                'harga_tiket'=> $film->harga_tiket,
                'sinopsis'  => $film->sinopsis,
                'durasi'    => $film->durasi
            ];
        });

        return Inertia::render('Show', compact('film', 'image', 'studios', 'user', 'recomends'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function listIndex(){
        $films = film::all()->map(function($film){
            return [
                'id'    => $film->film_id,
                'judul' => $film->judul,
                'gambar_film' => asset('images/'.$film->gambar_film),
                'genre_id'  => $film->genre_id,
                'harga_tiket'=> $film->harga_tiket,
                'sinopsis'  => $film->sinopsis,
                'durasi'    => $film->durasi
            ];
        });

        return Inertia::render('Films', compact('films'));
    }

    public function pesanTiket(Request $request){
        // @dd($request->all());



        $request->validate([
            'studio' => 'required',
            'total_penonton' => 'required'
        ]);


        transaksi::create([
            'user_id'   => $request->user_id,
            'studio_id' => $request->studio,
            'total_penonton'    => $request->total_penonton,
            'film_id'   => $request->film_id,
            'status'    => 0
        ]);

        return back()->with('Success', 'Anda berhasil melakukan pemesanan silahkan periksa menu selesaikan pembayaran');
    }

    public function completePayment($id){
        $tikets = transaksi::where('user_id', $id)->where('status', 0)->join('films', 'transaksis.film_id', '=', 'films.film_id')->join('studios', 'transaksis.studio_id', '=', 'studios.studio_id')->get();
        $tikets2 = tiket::join('transaksis', 'transaksis.transaksi_id', '=', 'tikets.transaksi_id')->where('transaksis.user_id', $id)->join('films', 'films.film_id', '=', 'transaksis.film_id')->join('studios', 'transaksis.studio_id', '=', 'studios.studio_id')->get();

        // @dd($tikets2);

        return Inertia::render('FilmCart', compact('tikets', 'tikets2'));
    }

    public function purchase(Request $request, $id){
        // @dd($request->all());
        $request->validate([
            'jam'   => 'required'
        ]);
        $data = transaksi::where('transaksi_id', $id)->join('films', 'films.film_id', '=', 'transaksis.film_id')->first();
        $totalHarga = $data->harga_tiket * $data->total_penonton;

        $user = User::where('id', $data->user_id)->first();

        $saldoAkhir = $user->saldo - $totalHarga;


        if($saldoAkhir < 0){
            return back()->with('message', 'Maaf Koin anda Tidak Cukup');
        }else {
            $update = transaksi::where('transaksi_id', $id)->update(['status' => 1]);

            tiket::create([
                'transaksi_id' => $data->transaksi_id,
                'jam'          => $request->jam,
                'total_harga'  => $totalHarga
                ]);

                $user->update(['saldo'=> $saldoAkhir]);

        }
        
        return back()->with('success', 'berhasil membeli tiket');
    }
}
