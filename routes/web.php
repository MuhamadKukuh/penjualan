<?php

use App\Http\Controllers\adminController;
use App\Http\Controllers\userController;
use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return Inertia::render('Welcome', [
        'canLogin' => Route::has('login'),
        'canRegister' => Route::has('register'),
        'laravelVersion' => Application::VERSION,
        'phpVersion' => PHP_VERSION,
    ]);
});

Route::group(['middleware' => 'auth', config('jetstream.auth_session'),
'verified'], function(){
    Route::get('/dashboard', [adminController::class, 'index'])->name('dashboard');
    Route::get('/tambah-film', function(){
        return Inertia::render('AddFilm');
    });
    Route::post('/tambah-buku', [adminController::class, 'store']);
    Route::post('/update/{id}', [adminController::class, 'update']);
    Route::get('/edit/{id}', [adminController::class, 'edit']);
    Route::get('/hapus/{id}', [adminController::class, 'destroy']);

    
    Route::get('/film/{id}', [userController::class, 'show']);
    Route::get('/home', [userController::class, 'index'])->name('home');
    Route::get('/list-film', [userController::class, 'listIndex'])->name('list-film');
    Route::post('/beliTiket', [userController::class, 'pesanTiket']);
    Route::get('/tiket/{id}', [userController::class, 'completePayment'])->name('myTiket');
    Route::post('/beliTiket/{id}', [userController::class, 'purchase']);
});